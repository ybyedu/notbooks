## 一、Docker安装

#### 查看 CentOS 内核版本

Docker 要求 CentOS 系统的内核版本高于`3.10`, 执行如下命令查询 内核版本

```shell
uname -r # 查看内核版本
3.10.0-957.21.3.el7.x86_64 # 输出结果
```

### 删除旧版本

```shell
# 先卸载旧版本的 docker
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```

### 安装 Docker-ec版

#### 安装依赖包

```shell
# yum-utils 提供了 yum-config-manager ，
# 并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。
yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
```

#### 设置仓库地址

1. 使用官方仓库

   ```shell
    
   yum-config-manager \
       --add-repo \
       https://download.docker.com/linux/centos/docker-ce.repo
   ```

2. 阿里云地址

   ```shell
   # 由于官方仓库是国外地址，一般较慢，所以可以设置国内的仓库地址
   yum-config-manager \
       --add-repo \
       http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   ```

#### 安装最新Docker

```shell
# 此种情况安装表示安装最新版本的 docker
yum install docker-ce docker-ce-cli containerd.io
```

#### 指定版本安装

1. 通过如下命令 列出可用版本

   ```shell
    # 列出可用版本
    yum list docker-ce --showduplicates | sort -r
   ```

2. 指定版本安装

   ```shell
   # VERSION_STRING 为前一个 命令列出的信息的 第二列的从':' 到 '-' 之间的数据
   yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
   ```

#### 启动Docker

Docker安装后默认是未启动的，可通过如下名启动Docker

```shell
systemctl start docker
```

### 卸载Docker

#### 删除安装包

```shell
yum remove docker-ce docker-ce-cli containerd.io
```

#### 删除镜像文件

```shell
rm -rf /var/lib/docker      # 删除镜像，配置等信息
rm -rf /var/lib/containerd  # 删除容器信息  
```

### 镜像加速

由于官方的镜像较慢，可以使用阿里云的镜像加速，执行如下命令即可，

```shell
# 阿里镜像加速 推荐安装 1.10.0 以上版本的 Docker
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://1bs4ljno.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```



## 二、Docker容器的使用

### Docker客户端

#### 查看 Docker 支持的命令

```shell
# 在命令行中直接执行下面两个中的任意一个即可
docker 
docker help
```

#### 查看某个命令的使用帮助

```shell
# 执行 命令 --help 的方式
docker run --help # 查看 run 命令的使用方法
```

### 使用容器

#### 获取镜像

```shell
# 通过 pull 命令拉去一个 centos 的镜像。
# 若未指定版本，则拉去最新的版本的镜像
docker pull centos # 拉去最新版本镜像
```

#### 运行容器

```shell
# 运行刚刚拉取的 centos 镜像。执行 run 命令，本地不存在运行的镜像，则会先去仓库拉取，然后再运行
docker run -it centos /bin/bash

#参数说明：
# -i          交互模式执行
# -t          终端
# -d          后台运行容器
# -P          容器内部端口随机映射到主机的端口
# -p          容器内部端口绑定到指定的主机端口
# -v          挂在卷，即指定一个宿主机的目录与挂在到容器中的某个目录上
# centos      centos镜像
# /bin/bash   放在镜像后面的就是镜像里面的命令
```

#### 查看容器

```shell
# 执行如下命令可以查所有的容器
docker ps -a
```

#### 退出容器

```shell
# 进入容器后执行 exit 即可退出容器,此命令在退出命令的同时也停止了容器
exit
```

#### start 启动

```shell
# 通过 start 命令启动一个停止的容器，后面可以跟 容器ID 或 容器名称
docker start <id 或 name>
```

> 说明： run 命令和 start命令的区别是：
>
> docker run     命令是通过一个 镜像模板启动一个新的容器
>
> docker start   命令是启动一个已经存在的停止的容器

#### 停止容器

```shell
# 停止一个容器
docker stop <ID 或 name>
```

#### 重启容器

```shell
# 重启一个容器  # ID 或 name 为容器的名字
docker restart  <ID 或 name>  # ID 或 name 为容器的名字
```

#### 设置开机自启动

```shell
docker container update --restart=always  <ID 或 name> # 容器ID 或 容器名称
```

#### 进入容器

在使用 **`-d`** 参数时，容器启动后会进入后台。此时想要进入容器，可以通过以下指令进入：

- docker attach
- docker exec    推荐大家使用 docker exec 命令，因为此退出容器终端，不会导致容器的停止。

**attach**

```shell
# 通过此命令进入容器，退出时，会停止容器
docker attach <ID 或 name>
```

**exec**

```shell
# 通过此命令进入容器 退出时不会停止容器
docker exec -it <ID 或 name> /bin/bash
```

#### 删除容器

```shell
# 删除容器
docker rm -f <ID 或 name>
```



## 三、Docker镜像使用

当运行容器时，如果本地不存在镜像，Docker则自动从镜像库中拉取镜像。

### 列出本地镜像

```shell
docker images # 通过此命令可以查看本地已有的镜像
# 输出结果如下
REPOSITORY    TAG       IMAGE ID       CREATED       SIZE
redis         latest    7faaec683238   9 days ago    113MB
hello-world   latest    feb5d9fea6a5   3 weeks ago   13.3kB
centos        latest    5d0da3dc9764   5 weeks ago   231MB

# 参数说明
# --all, -a  显示所有镜像
# --digests  显示签名信息
# --filter, -f  设置过滤器
# -q, --quiet 只列出镜像ID
```

### 查找镜像

1. 我们可以从 [Docker Hub]( https://hub.docker.com/) 网站来搜索镜像。

2. 通过命令查找镜像

   ```shell
    docker search <image> # 可以通过此命令查找镜像
    #如 查找mysql 的镜像
    docker search mysql
   ```

### 拉取镜像

可以通过如下名在镜像仓库中拉取镜像

```shell
docker pull <image>:<tag> # 拉取镜像
# image 镜像名称
# tag   指定要拉取的镜像 tag,  如果设置 tag 默认拉取 latest 版本

# 例如
docker pull mysql     # 拉取最新版本的 mysql 镜像
docker pull mysql:5.7 # 拉取5.7 版本的mysql 镜像 
```

### 删除镜像

镜像删除使用 **docker rmi** 命令，

```shell
docker rmi hello-world #删除 hello-world 镜像
#删除所有镜像可以使用如下名
docker rmi $(docker images -aq)
```

1. 当删除某个镜像出现如下错误时，可以添加 `-f` 参数 强制删除镜像

> Error response from daemon: conflict: unable to remove repository reference "redis" (must force) - container c92e01e4d913 is using its referenced image 7faaec683238

2. 当出现如下错误时，先把停止掉镜像对应的容器后，再添加 `-f`进行强制删除

> Error response from daemon: conflict: unable to delete 7faaec683238 (cannot be forced) - image is being used by running container c92e01e4d913

### 创建镜像

#### 在已有镜像上更新

1. 下载镜像

   ```shell
   docker pull centos # 下载一个centos镜像
   ```

2. 更新镜像

   ```shell
   docker run -it centos /bin/bash # 运行一个容器 
   
   yum update  # 更新 yum
   
   yum -y install net-tools # 在centos 中安装 net-tools 工具
   ```

3. 提交镜像

   使用 `docker commit` 命令提交刚才安装后的容器，为一个镜像

   ```shell
   docker commit -a "whycode" -m "install net-tools" a78275da1084 whycode/centos:v1
   # -a 指定作者
   # -m 描述信息
   # a78275da1084 刚才更新的容器ID
   # whycode/centos:v1  指定镜像名称  v1 标识 tag 
   ```

#### 通过Dockerfile构建新镜像

使用命令 `docker build `， 通过构建` Dockerfile` 文件，创建一个新的镜像文件

1. 创建 Dockerfile 文件

   ```shell
   FROM centos                     # 使用 centos 作为基础镜像
   RUN  mkdir whycode              # 创建一个 文件夹
   RUN  yum -y install net-tools   # 安装 net-tools 工具
   EXPOSE 80                       # 暴露 80 端口
   CMD   /bin/bash                 # 容器启动执行命令
   ```

2. 构建

   ```shell
   docker build -t whycode/centos:v2 . # 开始构建
   # -t  指明镜像名称， 如果在 构建时没有指定此项，则会生产“悬挂”镜像
   # .   Dockerfile 文件所在目录，可以指定Dockerfile 的绝对路径
   ```



